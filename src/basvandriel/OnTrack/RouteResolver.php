<?php

    namespace basvandriel\OnTrack;

    use basvandriel\OnTrack\Route\Matcher\HostPatternMatcher;
    use basvandriel\OnTrack\Route\Matcher\PathPatternMatcher;
    use basvandriel\OnTrack\Route\Parameters\ParametersResolver;
    use basvandriel\OnTrack\URI\URI;

    /**
     * Matches mapped URL's to the user-requested URL
     *
     * @package basvandriel\OnTrack
     */
    class RouteResolver
    {
        /**
         * @var URI $requestedURI The user-requested URI
         */
        private $requestedURI;

        /**
         * @var
         */
        private $dataTypes;

        /**
         * Matcher constructor.
         *
         * @param URI $requestedURI The user-requested URI
         */
        public function __construct(URI $requestedURI)
        {
            $this->requestedURI = $requestedURI;
            $this->dataTypes = $this->loadDefaultDataTypes();
        }

        /**
         * Matches the user-given URI to one of the mapped routes and returns the route content
         *
         * @param string   $currentRequestMethod
         * @param string   $expectedRequestMethod
         * @param array    $patterns The host and path route patterns
         * @param callable $action   The action that will get executed if the route matches the user-given URI
         *
         * @return string The content from the matched route
         *
         * @throws \basvandriel\OnTrack\RoutingMatchException
         */
        public function resolveRouteContent(string $currentRequestMethod, string $expectedRequestMethod, array $patterns, callable $action) : string
        {
            $matches = $this->resolvePatternMatches(
                $patterns,
                $this->dataTypes
            );

            if ($this->isRouteMatch($currentRequestMethod, $expectedRequestMethod, $matches)) {
                $arguments = (new \basvandriel\OnTrack\URI\Arguments\ArgumentsResolver())->resolveArguments($matches);

                $content = $action($arguments);
                if(!is_string($content)) {
                    throw new RoutingMatchException("Action should be a string!");
                }
                return $action($arguments);
            }

            return "";
        }

        /**
         * @param array $patterns
         *
         * @param array $dataTypes
         *
         * @return array
         */
        private function resolvePatternMatches(array $patterns, array $dataTypes) : array
        {
            $matches = array();

            # host patterns is optional
            $hostPatterns = array();

            if (isset($patterns['host_patterns'])) {
                $hostPatterns = $patterns['host_patterns'];
            }

            foreach ($hostPatterns as $hostPattern) {
                if (isset($matches['host']) && !empty($matches['host'])) {
                    break;
                }
                $resolvedHostPattern = (new ParametersResolver($dataTypes))->resolveParameterExpressions(
                    trim($hostPattern, "/")
                );

                # Find the matching occurences between the URI and the route pattern
                $matches['host'] = (new HostPatternMatcher())->matchPatterns(
                    $this->requestedURI,
                    $resolvedHostPattern
                );
            }

            # Resolving path pattern parameters
            foreach ($patterns['path_patterns'] as $pathPattern) {
                if (isset($matches['path']) && !empty($matches['path'])) {
                    break;
                }
                # Resolve any possible parameters into regular expressions
                $resolvedPathPattern = (new ParametersResolver($dataTypes))->resolveParameterExpressions(
                    trim($pathPattern, "/")
                );

                # Find the matching occurences between the URI and the route pattern
                $matches['path'] = (new PathPatternMatcher())->matchPatterns(
                    $this->requestedURI,
                    $resolvedPathPattern
                );
            }

            return $matches;
        }


        /**
         * @param string $currentRequestMethod
         * @param string $expectedRequestMethod
         * @param array  $matches
         *
         * @return bool
         */
        private function isRouteMatch(string $currentRequestMethod, string $expectedRequestMethod, array $matches) : bool
        {
            if ($currentRequestMethod != $expectedRequestMethod || !isset($matches['path'])) {
                return false;
            }

            $matchingPath
                = count($matches['path']) == count(explode("/", trim($this->requestedURI->getPath(), "/")));

            return ((isset($matches['host']) && !empty($matchingHost)) && $matchingPath) || $matchingPath;
        }

        /**
         *
         */
        private function loadDefaultDataTypes()
        {
            /**
             *
             */
            $types = array(
                "integer" => array(
                    "int" => "\\d+"
                )
            );

            return $types;
        }


        public function loadParameterTypesFromCollection(\basvandriel\OnTrack\Route\Parameters\Type\ParameterTypeCollection $parameterTypeCollection)
        {
            $parameterTypes = $parameterTypeCollection->getTypes();
            foreach ($parameterTypes as $parameterTypeName => $parameterTypeDetails) {
                if (!isset($this->dataTypes[$parameterTypeName])) {
                    $this->dataTypes[$parameterTypeName] = array();
                }
                $this->dataTypes[$parameterTypeName][] = $parameterTypeDetails;
            }
        }
    }