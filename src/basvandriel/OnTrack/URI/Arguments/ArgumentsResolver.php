<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 27-2-2017
     * Time: 18:35
     */

    namespace basvandriel\OnTrack\URI\Arguments;


    class ArgumentsResolver
    {
        /**
         * @param array $matches
         *
         * @return \basvandriel\OnTrack\URI\Arguments\Arguments
         */
        public function resolveArguments(array $matches) : Arguments
        {
            $arguments = new Arguments();

            if (!isset($matches['path'])) {
                return $arguments;
            }

            if(!isset($matches['host'])) {
                $matches['host'] = array();
            }

            /**
             *
             */
            foreach ($matches['host'] as $hostMatchIndex => $hostMatchArguments) {
                if (empty($hostMatchArguments)) {
                    continue;
                }
                $hostMatchArguments = array_filter(
                    $hostMatchArguments,
                    function($hostMatchValue) {
                        return is_string($hostMatchValue);
                    },
                    ARRAY_FILTER_USE_KEY
                );

                foreach ($hostMatchArguments as $hostMatchArgumentName => $hostMatchArgumentValue) {
                    $arguments->add('host', $hostMatchArgumentName, $hostMatchArgumentValue);
                }
            }

            /**
             *
             */
            foreach ($matches['path'] as $pathMatchIndex => $pathMatchArguments) {
                if (empty($pathMatchArguments)) {
                    continue;
                }
                $pathMatchArguments = array_filter(
                    $pathMatchArguments,
                    function($pathMatchValue) {
                        return is_string($pathMatchValue);
                    },
                    ARRAY_FILTER_USE_KEY
                );

                foreach ($pathMatchArguments as $pathMatchArgumentName => $pathMatchArgumentValue) {
                    $arguments->add('path', $pathMatchArgumentName, $pathMatchArgumentValue);
                }
            }

            return $arguments;
        }
    }