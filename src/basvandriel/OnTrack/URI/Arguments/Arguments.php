<?php
    namespace basvandriel\OnTrack\URI\Arguments;

    /**
     * Class Arguments
     *
     * @package basvandriel\OnTrack\URI\Arguments
     */
    class Arguments
    {
        /**
         * @var
         */
        private $data;

        /**
         * Arguments constructor.
         */
        function __construct()
        {
            $this->data = array();
        }

        /**
         * @param string $section
         * @param string $name
         * @param        $value
         */
        public function add(string $section, string $name, $value)
        {
            if (isset($this->data[$section][$name])) {
                throw new \InvalidArgumentException("Duplicate argument found!");
            }
            $this->data[$section][$name] = $value;
        }

        /**
         * @param string $section
         * @param string $name
         *
         * @return mixed
         */
        public function get(string $section, string $name)
        {
            if (!isset($this->data[$section][$name])) {
                return "";
            }

            return $this->data[$section][$name];
        }

        /**
         * @param string $section
         * @param string $name
         */
        public function delete(string $section, string $name)
        {
            if (!isset($this->data[$name])) {
                return;
            }
            unset($this->data[$section][$name]);
        }
    }