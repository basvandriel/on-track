<?php
    namespace basvandriel\OnTrack\URI;

    /**
     * The user-requested URI
     *
     * @package basvandriel\OnTrack\URI
     */
    class URI
    {
        /**
         * @var array $parsedURI The complete user-requested URI with parameters.
         *
         * @example http://username:password@example.com:8080/path?arg=value#anchor
         */
        private $parsedURI;

        /**
         * @var \basvandriel\OnTrack\URI\Arguments\Arguments $arguments The user-given arguments for matching with the
         *      requested URI.
         *
         * @example http://www.example.com/{i:id}-{i:anotherOne}/
         */
        private $arguments;

        /**
         * Creates a new URI object
         *
         * @param string $uriString The complete user-requested URI.
         *                          An example would be:
         *                          http://username:password@example.com:8080/path?arg=value#anchor
         */
        function __construct(string $uriString)
        {
            $this->parsedURI = parse_url(rtrim($uriString, "/"));
            $this->arguments = new \basvandriel\OnTrack\URI\Arguments\Arguments();
        }

        /**
         * Retrieves the host of the URI is on
         *
         * @return string The host of the URI is on
         */
        public function getHost() : string
        {
            if (!isset($this->parsedURI['host'])) {
                return "";
            }

            return $this->parsedURI['host'];
        }

        /**
         * Retrieves the requested URI path after the host
         *
         * @return string The requested URI path after the host
         */
        public function getPath() : string
        {
            if (!isset($this->parsedURI['path'])) {
                return "";
            }

            return $this->parsedURI['path'];
        }

        /**
         * @return \basvandriel\OnTrack\URI\Arguments\Arguments
         */
        public function getArguments()
        {
            return $this->arguments;
        }
    }