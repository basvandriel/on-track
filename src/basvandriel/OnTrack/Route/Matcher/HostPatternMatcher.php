<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 1-3-2017
     * Time: 16:21
     */

    namespace basvandriel\OnTrack\Route\Matcher;


    class HostPatternMatcher implements PatternMatcher
    {

        /**
         * Matches the user-requestd URI to a route pattern
         *
         * @param \basvandriel\OnTrack\URI\URI $URI
         * @param string                       $routePattern
         *
         * @return array
         */
        public function matchPatterns(\basvandriel\OnTrack\URI\URI $URI, string $routePattern) : array
        {
            if ($URI == null || $routePattern == "") {
                return array();
            }
            # Check if the $comparingResolvedPathPatternPart and $comparingRequestedUriPart matches
            # by performing the regular expression on the $comparingRequestedUriPart
            if (!preg_match("/^" . $routePattern . "$/", $URI->getHost(), $arguments)) {
                return array();
            }

            # If the arguments array is not associative, empty the array
            if (array_keys($arguments) == range(0, count($arguments) - 1)) {
                $arguments = array();
            }

            return array($arguments);
        }
    }