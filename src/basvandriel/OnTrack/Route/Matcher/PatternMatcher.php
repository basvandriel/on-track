<?php
    namespace basvandriel\OnTrack\Route\Matcher;

    /**
     * Interface PatternsMatcher
     *
     * @package basvandriel\OnTrack
     */
    interface PatternMatcher
    {

        /**
         * Matches the user-requestd URI to a route pattern
         *
         * @param \basvandriel\OnTrack\URI\URI $URI
         * @param string                       $routePattern
         *
         * @return array
         */
        public function matchPatterns(\basvandriel\OnTrack\URI\URI $URI, string $routePattern) : array;
    }