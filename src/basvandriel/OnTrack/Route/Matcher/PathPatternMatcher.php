<?php
    namespace basvandriel\OnTrack\Route\Matcher;

    class PathPatternMatcher implements PatternMatcher
    {

        /**
         * Matches the user-requestd URI to a route pattern
         *
         * @param \basvandriel\OnTrack\URI\URI $URI
         * @param string                       $routePattern
         *
         * @return array
         */
        public function matchPatterns(\basvandriel\OnTrack\URI\URI $URI, string $routePattern) : array
        {
            $results = array();

            if ($URI == null) {
                return array();
            }

            # Explode the route path pattern by the '/' character
            $pathRoutePatternParts = explode("/", $routePattern);

            # Explode the current uri path by the '/' character when it's trimmed
            $requestedUriParts = explode("/", trim($URI->getPath(), "/"));

            # Save how many parts the exploded current uri has
            $requestedUriSize = count($requestedUriParts);

            # Arrays has to be the same length
            if (count($pathRoutePatternParts) != $requestedUriSize) {
                return array();
            }

            # Start comparing
            for ($requestedUriSizeIndex = 0; $requestedUriSizeIndex < $requestedUriSize; $requestedUriSizeIndex++) {
                # The regular expression for matching with the user-requested URI
                $comparingResolvedPathPatternPart = "/^" . $pathRoutePatternParts[$requestedUriSizeIndex] . "$/";
                $comparingRequestedUriPart = $requestedUriParts[$requestedUriSizeIndex];

                # Check if the $comparingResolvedPathPatternPart and $comparingRequestedUriPart matches
                # by performing the regular expression on the $comparingRequestedUriPart
                if (!preg_match($comparingResolvedPathPatternPart, $comparingRequestedUriPart, $arguments)) {
                    return array();
                }

                # If the arguments array is not associative, empty the array
                if (array_keys($arguments) == range(0, count($arguments) - 1)) {
                    $arguments = array();
                }
                $results[] = $arguments;
            }

            return $results;
        }
    }