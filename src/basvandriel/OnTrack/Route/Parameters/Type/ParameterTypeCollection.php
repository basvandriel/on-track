<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 6-4-2017
     * Time: 14:56
     */

    namespace basvandriel\OnTrack\Route\Parameters\Type;

    /**
     * Class ParameterTypeCollection
     *
     * @package basvandriel\OnTrack\Route\Parameters\Type
     */
    class ParameterTypeCollection
    {

        /**
         * @var
         */
        private $parameterTypes;

        /**
         * ParameterTypeCollection constructor.
         */
        public function __construct()
        {
            $this->parameterTypes = array();
        }

        /**
         * @param string $dataType
         * @param string $dataTypeName
         */
        public function delete(string $dataType, string $dataTypeName)
        {

        }

        /**
         * @param string $dataType
         * @param string $dataTypeName
         * @param string $dataTypeRegularExpression
         */
        public function add(string $dataType, string $dataTypeName, string $dataTypeRegularExpression)
        {
            $allowedDataTypes = array(
                "boolean",
                "integer",
                "double",
                "string",
                "array",
                "object",
                "resource",
                "NULL"
            );
            if (!in_array($dataType, $allowedDataTypes)) {
                throw new \InvalidArgumentException("Invalid argument");
            }

            if (!isset($this->parameterTypes[$dataType])) {
                $this->parameterTypes[$dataType];
            }

            $this->parameterTypes[$dataType][] = array($dataTypeName => $dataTypeRegularExpression);
        }

        /**
         * @return array
         */
        public function getTypes()
        {
            return $this->parameterTypes;
        }
    }