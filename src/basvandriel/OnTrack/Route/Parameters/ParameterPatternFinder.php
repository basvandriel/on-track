<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 27-2-2017
     * Time: 15:16
     */

    namespace basvandriel\OnTrack\Route\Parameters;


    class ParameterPatternFinder
    {

        /**
         *
         */
        private const PARAMETERS_REGULAR_EXPRESSION
            = "/(?:{[a-zA-Z]{1,4}:[a-zA-Z_]*}){2,}(*SKIP)(*F)|"
              . "{(?<key>[a-zA-Z]{1,4}):"
              . "(?<name>[a-zA-Z_]+\\b)}/";


        /**
         * @param string $pattern
         *
         * @return array
         */
        public function findParameterPatterns(string $pattern) : array
        {
            preg_match_all(
                self::PARAMETERS_REGULAR_EXPRESSION,
                $pattern,
                $resolvedParameterPatterns,
                PREG_SET_ORDER
            );

            /*
             * Filter the array by removing all the unnessacery array keys
             */
            array_walk(
                $resolvedParameterPatterns,
                function(&$pattern) {
                    $pattern = array_filter(
                        $pattern,
                        function($pattern) {
                            return is_string($pattern) || $pattern == 0;
                        },
                        ARRAY_FILTER_USE_KEY
                    );
                }
            );

            return $resolvedParameterPatterns;
        }
    }