<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 27-2-2017
     * Time: 15:38
     */

    namespace basvandriel\OnTrack\Route\Parameters\Conflict;

    use basvandriel\OnTrack\Route\Parameters\ParameterPatternFinder;

    class PreviousResolvedParameterPatternsConflictHandler implements ParameterConflictHandler
    {


        /**
         * @param array  $resolvedPatternParts
         * @param string $currentParameterString
         *
         * @return bool|mixed
         */
        public function hasParameterConflict(array $resolvedPatternParts, string $currentParameterString) : bool
        {
            $hasConflict = false;

            /*
             * Loop through the current added resolved path pattern parts if
             * a duplicate has been found
             */
            foreach ($resolvedPatternParts as $resolvedPatternPart) {
                $parameterPatternsFinder = new ParameterPatternFinder();
                $resolvedParameterPatterns
                    = $parameterPatternsFinder->findParameterPatterns($resolvedPatternPart);

                if (in_array($currentParameterString, $resolvedParameterPatterns)) {
                    $hasConflict = true;
                    break;
                }
            }

            return $hasConflict;
        }
    }