<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 27-2-2017
     * Time: 15:36
     */

    namespace basvandriel\OnTrack\Route\Parameters\Conflict;


    interface ParameterConflictHandler
    {
        /**
         * @param array  $resolvedPatternParts
         * @param string $currentParameterString
         *
         * @return bool|mixed
         */
        public function hasParameterConflict(array $resolvedPatternParts, string $currentParameterString) : bool;
    }