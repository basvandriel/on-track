<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 27-2-2017
     * Time: 15:40
     */

    namespace basvandriel\OnTrack\Route\Parameters\Conflict;

    use basvandriel\OnTrack\Route\Parameters\ParameterPatternFinder;

    class CurrentResolvedParameterPatternConflictHandler
        implements \basvandriel\OnTrack\Route\Parameters\Conflict\ParameterConflictHandler
    {

        /**
         * @param array  $resolvedPatternParts
         * @param string $currentParameterString
         *
         * @return bool|mixed
         */
        public function hasParameterConflict(array $resolvedPatternParts, string $currentParameterString) : bool
        {
            $parameterPatternsFinder = new ParameterPatternFinder();
            $resolvedParameterPatterns
                = $parameterPatternsFinder->findParameterPatterns($currentParameterString);

            return in_array($currentParameterString, $resolvedParameterPatterns);
        }
    }