<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 27-2-2017
     * Time: 15:10
     */

    namespace basvandriel\OnTrack\Route\Parameters;

    use basvandriel\OnTrack\Route\Parameters\Conflict\CurrentResolvedParameterPatternConflictHandler;
    use basvandriel\OnTrack\Route\Parameters\Conflict\PreviousResolvedParameterPatternsConflictHandler;

    class ParametersResolver
    {

        /**
         * @var
         */
        private $dataTypes;

        /**
         * ParametersResolver constructor.
         *
         * @param array $dataTypes
         */
        public function __construct($dataTypes)
        {
            $this->dataTypes = $dataTypes;
        }

        /**
         * @param string $patternString
         *
         * @return string
         */
        public function resolveParameterExpressions(string $patternString) : string
        {
            /*
             * An array with parts of the parameters with
             * resolved parameters into the corresponding regular expressions
             */
            $resolvedPatternParts = array();

            # An array of the pattern splitted in pieces by the '/'character
            $patternParts = explode("/", $patternString);
            foreach ($patternParts as $patternPart) {
                /*
                 * The to be resolved pattern part with replaced parameter
                 * patterns into the parameter regular expressions with type
                 */
                $resolvedPatternPart = $patternPart;

                # Find the regular expressions
                $parameterPatternsFinder = new ParameterPatternFinder();
                $parameterPatterns
                    = $parameterPatternsFinder->findParameterPatterns($patternPart);

                /*
                 * If there are no parameter patterns found, just return the string itself
                 */
                if (empty($parameterPatterns)) {
                    $resolvedPatternParts[] = "(?:" . $patternPart . ")";
                    continue;
                }

                /*
                 * A collection of the resolved parameter patterns into
                 * regular expressions
                 */
                $resolvedParameterPatterns = array();

                # Resolve the parameter pattern regular expressions
                $parameterPatternResolver = new ParameterPatternResolver($this->dataTypes);
                foreach ($parameterPatterns as $parameterPattern) {
                    list($parameterString, $parameterType, $parameterName) = array_values($parameterPattern);

                    # Resolve the parameter pattern into a regular expression
                    $resolvedParameterPattern = $parameterPatternResolver->resolveParameterPattern(
                        $parameterType,
                        $parameterName
                    );

                    # Conflict checker
                    $parameterConflictHandlers = array(
                        new PreviousResolvedParameterPatternsConflictHandler(),
                        new CurrentResolvedParameterPatternConflictHandler()
                    );

                    $hasConflict = false;

                    /**
                     * @var \basvandriel\OnTrack\Route\Parameters\Conflict\ParameterConflictHandler $parameterConflictHandler
                     */
                    foreach ($parameterConflictHandlers as $parameterConflictHandler) {
                        $hasConflict = $parameterConflictHandler->hasParameterConflict(
                            $resolvedPatternParts,
                            $parameterString
                        );
                        if ($hasConflict) {
                            break;
                        }
                    }

                    if ($hasConflict) {
                        continue;
                    }

                    $resolvedPatternPart = str_replace(
                        $parameterString,
                        $resolvedParameterPattern,
                        $resolvedPatternPart
                    );
                    # Add the parameter pattern regular expression sto the collection
                    $resolvedParameterPatterns[] = $resolvedParameterPattern;
                }

                # Escape the special characters from the route pattern
                $formatter = new \basvandriel\OnTrack\Route\Formatter\EscapedCharactersFormatter();
                $resolvedPatternPart = $formatter->format(
                    array("resolvedParameterPatterns" => $resolvedParameterPatterns),
                    $resolvedPatternPart
                );

                $resolvedPatternParts[] = $resolvedPatternPart;
            }

            return implode("/", $resolvedPatternParts);
        }
    }