<?php
    /**
     * Created by IntelliJ IDEA.
     * User: Bas
     * Date: 27-2-2017
     * Time: 15:25
     */

    namespace basvandriel\OnTrack\Route\Parameters;


    class ParameterPatternResolver
    {

        private $parameterTypes;

        /**
         * ParameterPatternResolver constructor.
         *
         * @param array $parameterDataTypes
         */
        public function __construct(array $parameterDataTypes)
        {
            $this->parameterTypes = $parameterDataTypes;
        }

        /**
         * @param string $parameterDataType
         * @param string $parameterName
         *
         * @return string
         */
        public function resolveParameterPattern(string $parameterDataType, string $parameterName) : string
        {
            # Check if the type has been defined, otherwise leave it as an empty string
            $parameterDataTypeRegularExpression = "";

            foreach ($this->parameterTypes as $parameterTypeDataTypeName => $parameterTypeDetails) {
                if (!isset($parameterTypeDetails[$parameterDataType])) {
                    continue;
                }
                $parameterDataTypeRegularExpression = $parameterTypeDetails[$parameterDataType];
            }

            # Build the regular expression with the parameter type and parameter name
            $resolvedParameterPatternRegularExpression = "(?<"
                                                         . $parameterName
                                                         . ">"
                                                         . $parameterDataTypeRegularExpression
                                                         . ")";


            //Check for conflicts
            return $resolvedParameterPatternRegularExpression;
        }

    }