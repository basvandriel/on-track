<?php
    namespace basvandriel\OnTrack\Route\Formatter;


    class EscapedCharactersFormatter implements Formatter
    {
        /**
         * All single regular expression characters that needs to be escaped in
         * regular expression format
         */
        private const ALL_SPECIAL_CHARACTERS_REGEX = "([\\\\\\^\\$\\.\\|\\?\\*\\+\\(\\)])";

        /**
         * @param array  $data
         *
         * @param string $pattern
         *
         * @return string
         */
        public function format(array $data, string $pattern) : string
        {
            # If the data is not present, return an empty string
            if (!isset($data['resolvedParameterPatterns'])) {
                throw new \InvalidArgumentException("");
            }

            # An colllection of resolved parameter patterns with
            # escaped special characters
            $resolvedParameterPatternsWithEscapedCharacters = array();

            /*
             * Escape all the special characters inside the
             * parameter pattern regular expressions
             */
            foreach ($data['resolvedParameterPatterns'] as $resolvedParameterPattern) {
                $resolvedParameterPatternWithEscapedCharacters = preg_replace(
                    "/" . self::ALL_SPECIAL_CHARACTERS_REGEX . "/",
                    "\\\\$0",
                    $resolvedParameterPattern
                );

                $resolvedParameterPatternsWithEscapedCharacters[]
                    = $resolvedParameterPatternWithEscapedCharacters;
            }

            /*
             * The regular expression for escaping the special characters
             * outside the parameter patterns in the
             * resolved parameter pattern
             */
            $replaceSpecialCharactersRegularExpression = "";

            /*
             * Loop through all the parameter patterns with escaped parameters and
             * resolve them into a regular expression for excluding them
             */
            foreach ($resolvedParameterPatternsWithEscapedCharacters as $resolvedParameterPatternWithEscapedCharacters) {
                $replaceSpecialCharactersRegularExpression .= "(?:";
                $replaceSpecialCharactersRegularExpression .= $resolvedParameterPatternWithEscapedCharacters;
                $replaceSpecialCharactersRegularExpression .= "){1,}(*SKIP)(*F)|";
            }

            # Add the pattern for replacing all the characters to be escaped
            $replaceSpecialCharactersRegularExpression .= self::ALL_SPECIAL_CHARACTERS_REGEX;

            # Make it a regular expression by adding the delimiters
            $replaceSpecialCharactersRegularExpression = "/" . $replaceSpecialCharactersRegularExpression . "/";

            # Replace them
            $pattern
                = preg_replace($replaceSpecialCharactersRegularExpression, "\\\\$0", $pattern);

            return $pattern;
        }
    }