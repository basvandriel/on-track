<?php
    namespace basvandriel\OnTrack\Route\Formatter;

    /**
     * Formats a route pattern based on given data
     *
     * @package basvandriel\OnTrack\Route\Parameters\Formatter
     */
    interface Formatter
    {

        /**
         * @param array  $data
         *
         * @param string $pattern
         *
         * @return string
         */
        public function format(array $data, string $pattern) : string;
    }