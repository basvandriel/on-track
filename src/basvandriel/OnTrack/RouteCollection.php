<?php

    namespace basvandriel\OnTrack;

    /**
     * A route collection holds the routes and can store or delete them
     *
     * @package basvandriel\OnTrack
     */
    class RouteCollection
    {
        /**
         * @var array $routes The possible routes for matching with user-requested URI
         */
        private $routes;

        /**
         * RouteCollection constructor.
         *
         * @param array $routes
         */
        function __construct(array $routes)
        {
            $this->routes = $routes;
        }


        public function map($requestMethod, array $patterns, callable $action)
        {


        }

        /**
         * @return array
         */
        public function getRoutes()
        {
            return $this->routes;
        }
    }
