# OnTrack
[![Build Status](https://travis-ci.org/basvandriel/on-track.svg?branch=master)](https://travis-ci.org/basvandriel/on-track)
[![license](https://img.shields.io/github/license/basvandriel/on-track.svg)](LICENSE.md)

This respository contains a URL routing engine written in PHP. When a route matches the user-given URL, user-created content can be loaded. 

## Requirements

* The `git` tool
* PHP >= 7.1
* The composer dependency manager tool

## Installation
The project is currently still in production, there are currently no further installation guidelines.

If your using Apache with `mod_rewite` enabled, create a `.htaccess` file in the root of your project directory. Paste in the following contents:

```apache 
# Turn on the rewite engine
RewriteEngine On

# Let the rewrite engine listen to the root of the page
RewriteBase /

# When the file name doesn't exist, just redirect to the index.php file
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

RewriteRule ^(.*?)$ index.php

# If an error occurs, always fall back on the index.php
FallbackResource index.php
```

## Usage
The project is currently still in production, there are currently no official usage guidelines.

## Contribution
Contributions to the project are currently closed - see the [CONTRIBUTION.md](CONTRIBUTION.md) file for details.

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Authors
This project was initially created by [Bas van Driel](https://github.com/basvandriel "GitHub page") ([@bvandriel](https://twitter.com/bvandriel "Twitter page")), where [these people](https://github.com/basvandriel/WWW/graphs/contributors) contributed to it.

## Links

* [Issue tracker](https://github.com/basvandriel/on-track/issues)
* [Source code](https://github.com/basvandriel/on-track)
