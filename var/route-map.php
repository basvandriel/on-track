<?php

    array(
        array(
            "inputURI"   => "example.com/blog/13",

            "pattern"   => "example.com/blog/{int:id}",
            "method" => "GET",
            "action" => function($request, $response) {
                return "";
            }
        ),

        array(
            "inputURI"   => "example.com/blog/13",

            "host_pattern"   => array("example.com", "sub.example.com"),
            "path_pattern"   => array("/", "/blog"),
            "method" => "GET",
            "action" => function($request, $response) {
                return "";
            }
        ),
        array(
            "inputURI" => "example.com/blog/13",

            "host_pattern" => array("example.com", "sub.example.com"),
            "path_pattern" => array("/", "/blog/{int:id}/version{int:id}19"),
            "method"       => "GET",
            "action"       => function() {

                return "";
            }
        ),
    );