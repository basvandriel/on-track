<?php

    namespace basvandriel\OnTrack;

    use basvandriel\OnTrack\URI\URI;
    use PHPUnit\Framework\TestCase;

    class RouteResolverTest extends TestCase
    {

        public function testMatchOK()
        {
            $URI = new URI("http://www.example.com/blog/13");
            $currentRequestMethod = "GET";

            $requestedRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/blog/13", "/blog/{int:hallo}")
            );
            $action = function() {
                return "Hello me!";
            };

            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $requestedRequestMethod,
                $patterns,
                $action
            );

            $this->assertEquals("Hello me!", $actual);
        }

        public function testWrongMatch()
        {
            $URI = new URI("http://www.example.com/blog/13");
            $currentRequestMethod = "GET";

            $requestedRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/blog/16")
            );
            $action = function() {
                return "Hello me!";
            };

            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $requestedRequestMethod,
                $patterns,
                $action
            );

            $this->assertEquals("", $actual);
        }

        public function testNoMatch()
        {
            $URI = new URI("http://www.example.com/blog/13");

            $currentRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/blog/515")
            );
            $action = function() {
                return "Hello me!";
            };
            $expectedRequestMethod = "POST";


            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $expectedRequestMethod,
                $patterns,
                $action
            );

            $this->assertEquals("", $actual);
        }

        public function testAnotherLongerMatch()
        {
            $URI = new URI("http://www.example.com/user/profile/1");

            $currentRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/user/profile/1")
            );
            $action = function() {
                return "Hello me!";
            };
            $expectedRequestMethod = "GET";

            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $expectedRequestMethod,
                $patterns,
                $action
            );

            $this->assertEquals("Hello me!", $actual);
        }

        public function testMatchWithParameterPattern()
        {
            $URI = new URI("http://www.example.com/user/profile/profile/15+12");

            $currentRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/user/profile/profile/{int:profileID}+{int:anotherone}")
            );
            $action = function() {
                return "Hello me!";
            };
            $expectedRequestMethod = "GET";


            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $expectedRequestMethod,
                $patterns,
                $action
            );

            $this->assertEquals("Hello me!", $actual);
        }

        public function testMatchWithParameterPatternAndEscapeChars()
        {
            $URI = new URI("http://www.example.com/user/profile/15-12");

            $currentRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/user/profile/{int:profileID}-{int:anotherone}")
            );
            $action = function() {
                return "Hello me!";
            };
            $expectedRequestMethod = "GET";


            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $expectedRequestMethod,
                $patterns,
                $action
            );

            $expectedResult = "Hello me!";

            $this->assertEquals($expectedResult, $actual);
        }

        public function testMatchWithRetreivingArgument()
        {
            $URI = new URI("http://www.example.com/user/profile/profile/12");

            $currentRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/user/profile/profile/{int:profileID}")
            );
            $action = function(\basvandriel\OnTrack\URI\Arguments\Arguments $arguments) {
                return $arguments->get('path', 'profileID');
            };
            $expectedRequestMethod = "GET";


            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $expectedRequestMethod,
                $patterns,
                $action
            );

            $this->assertEquals("12", $actual);
        }

        public function testMatchWithNonExistantParameterType()
        {
            $URI = new URI("http://www.example.com/user/profile/");

            $currentRequestMethod = "GET";
            $patterns = array(
                "host_patterns" => array("www.example.com"),
                "path_patterns" => array("/user/profile/{i:profileID}")
            );
            $action = function() {
                return "Hey!";
            };
            $expectedRequestMethod = "GET";


            $routeResolver = new RouteResolver($URI);
            $actual = $routeResolver->resolveRouteContent(
                $currentRequestMethod,
                $expectedRequestMethod,
                $patterns,
                $action
            );

            $this->assertEquals("", $actual);
        }
    }
