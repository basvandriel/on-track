<?php

    namespace basvandriel\OnTrack;

    use basvandriel\OnTrack\URI\URI;

    class URITest extends \PHPUnit\Framework\TestCase
    {
        public function testParseNonTrimmedURI()
        {
            $uri = new URI("http://www.example.com/hello/");
            $expected = "/hello";

            $this->assertEquals($expected, $uri->getPath());
        }

        public function testParseURI()
        {
            $uri = new URI("http://www.example.com/hello");
            $expected = "/hello";

            $this->assertEquals($expected, $uri->getPath());
        }

        public function testParseHost()
        {
            $uri = new URI("http://www.example.com/hello");
            $expected = "www.example.com";

            $this->assertEquals($expected, $uri->getHost());
        }
    }