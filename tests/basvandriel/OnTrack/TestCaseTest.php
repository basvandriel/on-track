<?php
    namespace basvandriel\OnTrack;

    /**
     * Class TestCaseTest
     *
     * @package basvandriel\OnTrack
     */
    class TestCaseTest extends \PHPUnit\Framework\TestCase
    {
        /**
         *
         */
        public function testIsTestClass()
        {
            $expected = is_a($this, "\\PHPUnit\\Framework\\TestCase");
            $this->assertTrue($expected);
        }
    }
