# `/tests` : Source code for testing

## Purpose

The `/tests` hierarchy contains the object-oriented application source code for testing parts of the application.

## Requirements

Only object-oriented PHP source code intended for testing compatible with the PSR-4 standard.

## Weblinks

* [PSR-0 Autoloading Standard](http://www.php-fig.org/psr/psr-0/)
* [Linux Foundation: FHS 2.3](http://refspecs.linuxfoundation.org/FHS_2.3/fhs-2.3.html#USRSRCSOURCECODE)